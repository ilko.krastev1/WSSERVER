package FileSystem;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class FileUtils {

    public FileUtils() {}

    public static Boolean append(String filename, String key, String msg) {
        try
        {
            FileWriter fw = new FileWriter("logs/"+filename, true); //the true will append the new data
            fw.write(new Date() + ": " + key + ": "+msg+"\r\n");//appends the string to the file
            fw.close();
        } catch(IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static Boolean write(String filename, String msg) {
        try
        {
            FileWriter fw = new FileWriter(filename); //the true will append the new data
            fw.write(msg); //appends the string to the file
            fw.close();
        } catch(IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static Boolean delete(String filename) {
        try {
            File f = new File(filename);
            f.delete();
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}

