package Launchers;

import Amqp.MBControler;
import Server.WSServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.FileInputStream;
import java.io.InputStream;

public class Launcher {
    static {
        try {
            InputStream inputStream = new FileInputStream("log4j2.xml");
            ConfigurationSource source = new ConfigurationSource(inputStream);
            Configurator.initialize(null, source);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    private static final Logger logger = LogManager.getLogger(Launcher.class);
    public static MBControler msgBrokerControler = null;

    private void run() {

        msgBrokerControler = new MBControler("config/amqp.conf");
        msgBrokerControler.loadConfig();
        msgBrokerControler.connect();

        WSServer wsServer = new WSServer(8888);
        wsServer.start();

    }

    public static void main(String[] args) throws Exception {
        logger.debug("Start WSServer");
        Launcher Launcher = new Launcher();
        Launcher.run();
    }


}


