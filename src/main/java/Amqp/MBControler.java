package Amqp;

import FileSystem.FileUtils;
import com.rabbitmq.client.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.TimeoutException;

public class MBControler {
    private Integer minConnections = 3;
    private Integer maxConnections = 10;
    private Integer maxFreeConnections = 5;
    private Properties properties = new Properties();
    private ConnectionFactory factory = new ConnectionFactory();
    public Vector<Connection> conPublishPool = new Vector<Connection>();
    private Vector<Connection> conConsumerPool = new Vector<Connection>();
    private Vector<Object> queueBindings = new Vector<Object>();
    private Vector<String> qmQueue = new Vector<String>();
    public String configFile = "conf/amqp.conf";
    public HashMap<String, Object>  config;

    public MBControler() {

    }

    public MBControler(String configFile)
    {
        this.configFile = configFile;
    }

    public void connect() {
        factory.setHost((String) config.get("mb_host"));
        factory.setPort((Integer) config.get("mb_port"));
        factory.setUsername((String) config.get("mb_username"));
        factory.setPassword((String) config.get("mb_password"));
        factory.setVirtualHost((String) config.get("mb_vhost"));
        factory.setRequestedHeartbeat(10);
        factory.setConnectionTimeout(2000);
        factory.setShutdownTimeout(1000);
        setConnetionPool(conPublishPool);
        setConnetionPool(conConsumerPool);
    }

    public HashMap<String, Object> loadConfig() {

        config = new HashMap<String, Object>();
        File f = new File(this.configFile);
        System.out.println("LOAD MBCONTROLER CONFIG FROM: " + f.getAbsolutePath());
        FileInputStream is;
        try {
            is = new FileInputStream(configFile);
            properties.load(is);
        } catch (IOException e) {
            System.out.println("Configuration file missing...!!!");
            return null;
        }

        Set<Object> s = properties.keySet();
        System.out.println(s.toString());

        String mb_host = properties.getProperty("mb_host");
        if (mb_host == null || mb_host.equalsIgnoreCase("")) {
            System.out.println("Missing variable 'mb_host'! It's a mandatory variable!!!");
            System.exit(1);
        }
        config.put("mb_host", mb_host);

        String port = properties.getProperty("mb_port");
        if (port == null || port.equalsIgnoreCase("")) {
            System.out.println("Missing variable 'mb_port'! It's a mandatory variable!!!");
            System.exit(1);
        }
        Integer mb_port = Integer.parseInt(port);
        config.put("mb_port", mb_port);

        String mb_username = properties.getProperty("mb_username");
        if (mb_username == null || mb_username.equalsIgnoreCase("")) {
            System.out.println("Missing variable 'mb_username'! It's a mandatory variable!!!");
            System.exit(1);
        }
        config.put("mb_username", mb_username);

        String mb_password = properties.getProperty("mb_password");
        if (mb_password == null || mb_password.equalsIgnoreCase("")) {
            System.out.println("Missing variable 'mb_password'! It's a mandatory variable!!!");
            System.exit(1);
        }
        config.put("mb_password", mb_password);

        String mb_vhost = properties.getProperty("mb_vhost");
        if (mb_vhost == null || mb_vhost.equalsIgnoreCase("")) {
            System.out.println("Missing variable 'mb_vhost'! It's a mandatory variable!!!");
            System.exit(1);
        }
        config.put("mb_vhost", mb_vhost);

        return config;
    }

    public void setConnetionPool(Object Pool)
    {
        Vector<Connection> ConnectionPool = (Vector<Connection>) Pool;
        System.out.println("INIT MB CONNECTION POOL");
        for(int i = 0 ; i < minConnections; i++)
        {
            Connection connection = connection();
            connection.setId("CONN-" + i);
            ConnectionPool.add(connection);
        }
    }

    public Connection connection() {
        try {
            Connection connection = factory.newConnection();
            ShutdownListener shutdownListener = new ShutdownListener() {
                @Override
                public void shutdownCompleted(ShutdownSignalException cause) {
                    System.out.println("shutdown connection: "+ cause.getMessage());
                }
            };
            connection.addShutdownListener(shutdownListener);

            return connection;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Connection getConnection(Object Pool) {
        Connection connection = null;
        Vector<Connection> ConnectionPool = (Vector<Connection>) Pool;

        if(ConnectionPool.size() > 0)
        {
            connection = ConnectionPool.get(0);
            if(connection != null && connection.isOpen()) {
                ConnectionPool.remove(0);

                return connection;
            } else if(connection != null){

                System.out.println("REMOVE AND CLOSE CONNECTION");
                ConnectionPool.remove(0);
            }
        }
        connection = connection();
        ConnectionPool.add(connection);

        return connection;
    }

    public void returnConnection(Connection connection, Object Pool) {
        //TODO FIX IT WITH CORECT LOGIC
        Vector<Connection> ConnectionPool = (Vector<Connection>) Pool;
        //System.out.println("ConnectionPool.size(): " + ConnectionPool.size() +" > "+ (minConnections + maxFreeConnections));

        if(ConnectionPool.size() > minConnections + maxFreeConnections) {
            disconect(connection);
        } else {
            ConnectionPool.add(connection);
        }
    }

    public void disconect(Connection connection) {
        try {
            connection.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*
    	HashMap
    	@exchange, @type, @durable, @autodelete, @arg
    */
    public void addExchange(HashMap<String, Object> hmConfig) {
        String exchange = (String) hmConfig.get("exchange");
        String type = (String) hmConfig.get("type");
        Boolean durable = hmConfig.get("durable") != null ? (Boolean) hmConfig.get("durable") : false;
        Boolean autodelete = hmConfig.get("autodelete") != null ? (Boolean) hmConfig.get("autodelete") : false;
        HashMap<String, Object> arg = (HashMap<String, Object>) hmConfig.get("arg");

        new Runnable() {
            @Override
            public void run() {
                Connection connection = getConnection(conPublishPool);
                try {
                    Channel channel = connection.createChannel();
                    channel.exchangeDeclare(exchange, type, durable, autodelete, arg);
                } catch(Exception e) {
                    e.printStackTrace();
                } finally {
                    returnConnection(connection, conPublishPool);
                }
            }
        }.run();

    }

    public void addQueue(HashMap<String, Object> hmConfig) {
        String queue = (String) hmConfig.get("queue");

        Boolean durable = hmConfig.get("durable") != null ? (Boolean) hmConfig.get("durable") : false;
        Boolean exclusive = hmConfig.get("exclusive") != null ? (Boolean) hmConfig.get("exclusive") : false;
        Boolean autodelete = hmConfig.get("autodelete") != null ? (Boolean) hmConfig.get("autodelete") : false;
        HashMap<String, Object> arg = hmConfig.get("arg") != null ? (HashMap<String, Object>) hmConfig.get("arg") : null;

        new Runnable() {
            @Override
            public void run() {
                Connection connection = getConnection(conPublishPool);
                try {
                    Channel channel = connection.createChannel();
                    channel.queueDeclare(queue, durable, exclusive, autodelete, arg);
                    channel.close();
                } catch(Exception e) {
                    e.printStackTrace();
                } finally {
                    returnConnection(connection, conPublishPool);
                }
            }
        }.run();

    }

    public void bindExchangeToExchange(HashMap<String, Object> hmConfig) throws Exception  {

        String dst_exchange = (String) hmConfig.get("dst_exchange");
        String src_exchange = (String) hmConfig.get("src_exchange");
        String routing_key = (String) hmConfig.get("routing_key");

        Connection connection = getConnection(conConsumerPool);
        try {
            Channel channel = connection.createChannel();

            channel.exchangeBind(dst_exchange, src_exchange, routing_key);
            channel.close();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        } finally {
            returnConnection(connection, conPublishPool);
        }
    }

    public void bindQueuetoExchange(HashMap<String, Object> hmConfig) throws Exception  {
        String exchange = (String) hmConfig.get("exchange");
        String queue = (String) hmConfig.get("queue");
        String routing_key = (String) hmConfig.get("routing_key");
        Connection connection = getConnection(conConsumerPool);
        try {
            Channel channel = connection.createChannel();
            channel.queueBind(queue, exchange, routing_key);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        } finally {
            returnConnection(connection, conPublishPool);
        }
    }

    public void publish(String exchange, String routing_key, String message, Boolean confirm) {

        new Runnable() {
            @Override
            public void run() {
                Connection connection = getConnection(conPublishPool);
                try {
                    Channel channel = connection.createChannel();

                    channel.addReturnListener(new ReturnListener() {
                        public void handleReturn(int replyCode, String replyText, String exchange, String routingKey, AMQP.BasicProperties properties, byte[] body)

                                throws IOException {
                            System.out.println(replyCode);
                            System.out.println(replyText);
                            System.out.println(exchange);
                            System.out.println(routingKey);
                        }
                    });

                    if(confirm == true)
                    {
                        channel.confirmSelect();
                        channel.basicPublish(exchange, routing_key, null, message.getBytes("UTF-8"));
                        channel.addConfirmListener(new ConfirmListener()
                        {
                            public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                                channel.basicPublish(exchange, routing_key, null, message.getBytes("UTF-8"));
                            }

                            public void handleAck(long deliveryTag, boolean multiple) throws IOException {
                                try {
                                    channel.close();
                                } catch (Exception e) {
                                }
                            }
                        });
                    } else {
                        channel.basicPublish(exchange, routing_key, null, message.getBytes("UTF-8"));
                        channel.close();
                    }

                } catch(Exception e) {
                    //Log message to file
                    setQueue(exchange, routing_key, message);
                    FileUtils.append(exchange, routing_key, message);
                    e.printStackTrace();
                } finally {
                    returnConnection(connection, conPublishPool);
                }
            }
        }.run();

    }

    public void tPublish(String exchange, String routing_key, String message, Boolean auto_delete) {
        //System.out.println("SEND " + routing_key + ": " + message);
        Connection connection = getConnection(conPublishPool);
        try {
            Channel channel = connection.createChannel();
            channel.confirmSelect();
            //channel.exchangeDeclare(exchange, "topic", true, auto_delete, null);
            channel.addConfirmListener(new ConfirmListener()
            {
                public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                    //System.out.println("Not ack received");
                }

                public void handleAck(long deliveryTag, boolean multiple) throws IOException {
                    //System.out.println("Ack received");
                }
            });

            channel.basicPublish(exchange, routing_key, null, message.getBytes("UTF-8"));
            channel.close();
        } catch(Exception e) {
            //Log message to file
            FileUtils.append(exchange, routing_key, message);
            e.printStackTrace();
        } finally {
            returnConnection(connection, conPublishPool);
        }
    }

    public void fPublish(String exchange, String routing_key, String message, Boolean auto_delete, Boolean addConfirmListener) {
        //System.out.println("SEND " + routing_key + ": " + message);
        Connection connection = getConnection(conPublishPool);
        try {
            Channel channel = connection.createChannel();
            channel.confirmSelect();
            //channel.exchangeDeclare(exchange, "fanout", true, auto_delete, null);
            //channel.exchangeDeclare(exchange, "fanout");
            channel.addConfirmListener(new ConfirmListener()
            {
                public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                    //System.out.println("Not ack received");
                }

                public void handleAck(long deliveryTag, boolean multiple) throws IOException {
                    //System.out.println("Ack received");
                }
            });

            channel.basicPublish(exchange, routing_key, null, message.getBytes("UTF-8"));
            channel.close();
        } catch(Exception e) {
            //Log message to file
            FileUtils.append(exchange, routing_key, message);
            e.printStackTrace();
        } finally {
            returnConnection(connection, conPublishPool);
        }
    }

    public void hPublish(String exchange, String routing_key, String message, Boolean auto_delete) {
        //System.out.println("SEND " + routing_key + ": " + message);
        Connection connection = getConnection(conPublishPool);
        try {
            Channel channel = connection.createChannel();
            channel.confirmSelect();
            //channel.exchangeDeclare(exchange, "headers", true, auto_delete, null);
            channel.addConfirmListener(new ConfirmListener()
            {
                public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                    //System.out.println("Not ack received");
                }

                public void handleAck(long deliveryTag, boolean multiple) throws IOException {
                    //System.out.println("Ack received");
                }
            });

            channel.basicPublish(exchange, routing_key, null, message.getBytes("UTF-8"));
            channel.close();

        } catch(Exception e) {
            //Log message to file
            FileUtils.append(exchange, routing_key, message);
            e.printStackTrace();
        } finally {
            returnConnection(connection, conPublishPool);
        }
    }

    public void dPublish(String exchange, String routing_key, String message, Boolean auto_delete, Boolean addConfirmListener) {

        new Runnable() {
            @Override
            public void run() {
                Connection connection = getConnection(conPublishPool);
                try {
                    Channel channel = connection.createChannel();

                    channel.addReturnListener(new ReturnListener() {
                        public void handleReturn(int replyCode, String replyText, String exchange, String routingKey, AMQP.BasicProperties properties, byte[] body)

                                throws IOException {
                            System.out.println(replyCode);
                            System.out.println(replyText);
                            System.out.println(exchange);
                            System.out.println(routingKey);
                        }
                    });

                    if(addConfirmListener == true)
                    {
                        channel.confirmSelect();
                        channel.basicPublish(exchange, routing_key, null, message.getBytes("UTF-8"));
                        channel.addConfirmListener(new ConfirmListener()
                        {
                            public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                                channel.basicPublish(exchange, routing_key, null, message.getBytes("UTF-8"));
                            }

                            public void handleAck(long deliveryTag, boolean multiple) throws IOException {
                                try {
                                    channel.close();
                                } catch (Exception e) {
                                }
                            }
                        });
                    } else {
                        channel.basicPublish(exchange, routing_key, null, message.getBytes("UTF-8"));
                        channel.close();
                    }

                } catch(Exception e) {
                    //Log message to file
                    setQueue(exchange, routing_key, message);
                    FileUtils.append(exchange, routing_key, message);
                    e.printStackTrace();
                } finally {
                    returnConnection(connection, conPublishPool);
                }
            }
        }.run();

    }

    public Channel consume(String queue, Object msgHandlerClass, String method) throws Exception {

        Connection connection = getConnection(conConsumerPool);
        Channel channel = connection.createChannel();

        channel.basicRecover();
        DeliverCallback deliverCallback = (consumerTag, delivery) ->
        {
            String message = new String(delivery.getBody(), "UTF-8");

            try {
                //refect to object
                Method setMethod = msgHandlerClass.getClass().getMethod(method, String.class);
                Object result = setMethod.invoke(msgHandlerClass, message);
                //result shoud be boolean
                if((boolean) result == true)
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), true);
                else {
                    channel.basicNack(delivery.getEnvelope().getDeliveryTag(), false, false);
                }

            } catch(Exception e) {
                e.printStackTrace();
                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            }
        };

        boolean autoAck = false;
        channel.basicConsume(queue, autoAck, deliverCallback, consumerTag -> { });

        returnConnection(connection, conConsumerPool);

        return channel;

    }

    public Vector<String> getQueue() {
        return qmQueue;
    }

    public void setQueue(String exchange, String routing_key, String message) {

        this.qmQueue.add(message);
    }

    public void doQueue(Channel channel, String exchange, String routing_key) {
        Vector<String> queue = getQueue();
        if(queue.size() > 0 )
        {
            for(int i = 0; i < queue.size(); i++) {
                try {
                    System.out.println("FLUSH QUEUE: "+ queue.get(i));
                    channel.basicPublish(exchange, routing_key, null, queue.get(i).getBytes("UTF-8"));
                    this.qmQueue.remove(i);
                } catch (Exception e) {

                }
            }
        }
    }
}

