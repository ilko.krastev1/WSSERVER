package Amqp.Consumers;

import Launchers.Launcher;

public class OpenSIPsConsumer {
    public Boolean suspend = true;
    public Boolean terminate = false;
    private String wsQueue = null;

    public OpenSIPsConsumer(String wsQueue) {
        this.wsQueue = wsQueue;
        try {
            Launcher.msgBrokerControler.consume(wsQueue, this, "Logic");
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
    }

    public Boolean Logic(String text)
    {
        try {
            System.out.println("Logic: "+ text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void suspend(Boolean suspend) {
        this.suspend = suspend;
    }

    public void terminate(Boolean terminate) {
        this.terminate = terminate;
    }
}
