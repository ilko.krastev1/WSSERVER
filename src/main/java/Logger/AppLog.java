package Logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppLog {

    private static Logger logger = LoggerFactory.getLogger(AppLog.class);

    public static Logger getLogger() {

        return logger;
    }
}