package Server;

import Launchers.Launcher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class WSServer extends WebSocketServer {
    private static final Logger logger = LogManager.getLogger(WSServer.class);
    private ConcurrentHashMap<String, String> hmCalls = new ConcurrentHashMap<String, String >();
    private ConcurrentHashMap<String, WebSocket> webSockets = new ConcurrentHashMap<String, WebSocket>();

    public WSServer(int port) {
        super( new InetSocketAddress("0.0.0.0", port));
        this.setReuseAddr(true);
    }

    @Override
    public void onOpen(WebSocket ws, ClientHandshake handshake) {
        logger.debug("WSS Client connected: " + handshake.getResourceDescriptor());
        String[] uri = handshake.getResourceDescriptor().split("/");
        JSONObject user = authenticate(uri[uri.length - 1]);
        System.out.println("USER: " + user);
        if(user != null)
            webSockets.put(user.get("extension_number") + "@" + user.get("domain_name"), ws);
        else
            ws.close();
    }

    @Override
    public void onClose(WebSocket ws, int code, String reason, boolean remote) {
        logger.debug("WSS Client disconnected: " + ws );
        try {
            webSockets.remove(getKeysByValue(webSockets, ws));
        } catch (Exception e) {
        }
    }

    @Override
    public void onMessage(WebSocket ws, String message) {
        logger.debug("NEW MSG ARRIVED");
    }

    @Override
    public void onError(WebSocket ws, Exception ex) {
        logger.debug(ex);
        try {
            webSockets.remove(getKeysByValue(webSockets, ws));
        } catch (Exception e) {
        }
    }

    @Override
    public void onStart()  {
        logger.debug("STARTED WSS SERVER");
        try {
            /**
             * TODO ADD LOGIC TO CREATE A QUEUE AND BIND IT TO EXCHANGE
             * */
            Launcher.msgBrokerControler.consume("calls", this, "runLogic");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public Boolean runLogic(String message)
    {
        try {
            Boolean forward = false;
            JSONObject json = new JSONObject(message);
            String number = (String) json.get("calling_number") + "@" + (String) json.get("fromdomain");;
            String event = (String) json.get("event");

            if(((String) json.get("calltype")).equalsIgnoreCase("out"))
            {
                number = (String) json.get("calling_number") + "@" + (String) json.get("fromdomain");
                String lastEvent = hmCalls.get(number);

                if(hmCalls.get(number) == null && event.equalsIgnoreCase("invite")) {
                    json.put("state", "CONNECTING");
                    hmCalls.put(number, event);
                    forward = true;
                } else if(lastEvent != null) {
                    if (lastEvent.equalsIgnoreCase("invite"))
                    if (event.equalsIgnoreCase("ack"))
                    {
                        json.put("state", "ANSWER");
                        hmCalls.put(number, event);
                        forward = true;
                    }

                    if (lastEvent.equalsIgnoreCase("ack") || lastEvent.equalsIgnoreCase("invite"))
                    if (event.equalsIgnoreCase("cancel") || event.equalsIgnoreCase("bye"))
                    {
                        json.put("state", "HANGUP");
                        hmCalls.put(number, event);
                        forward = true;
                    }

                    if (lastEvent.equalsIgnoreCase("bye") || lastEvent.equalsIgnoreCase("cancel"))
                    if (event.equalsIgnoreCase("ack"))
                    {
                        json.put("state", "CLOSED");
                        hmCalls.remove(number);
                        forward = true;
                    }
                }
            }

            if(((String) json.get("calltype")).equalsIgnoreCase("inc"))
            {
                number = (String) json.get("called_number") + "@" + (String) json.get("fromdomain");
                String lastEvent = hmCalls.get(number);

                if(hmCalls.get(number) == null && event.equalsIgnoreCase("invite")) {
                    json.put("state", "CONNECTING");
                    hmCalls.put(number, event);
                    forward = true;
                } else if(lastEvent != null) {
                    if (lastEvent.equalsIgnoreCase("invite"))
                    if (event.equalsIgnoreCase("ack"))
                    {
                        json.put("state", "ANSWER");
                        hmCalls.put(number, event);
                        forward = true;
                    }

                    if (lastEvent.equalsIgnoreCase("ack") || lastEvent.equalsIgnoreCase("invite"))
                    if (event.equalsIgnoreCase("cancel") || event.equalsIgnoreCase("bye"))
                    {
                        json.put("state", "HANGUP");
                        hmCalls.put(number, event);
                        forward = true;
                    }

                    if (lastEvent.equalsIgnoreCase("bye") || lastEvent.equalsIgnoreCase("cancel"))
                    if (event.equalsIgnoreCase("ack"))
                    {
                        json.put("state", "CLOSED");
                        hmCalls.remove(number);
                        forward = true;
                    }
                }
            }

            if(forward)
            {
                logger.debug(json);
                WebSocket ws = webSockets.get(number);
                if (ws != null)
                    ws.send(json.toString());
                else {
                    ws = webSockets.get("monitor");
                    ws.send(json.toString());
                    logger.debug("Client not found");
                    logger.warn(json);
                    return true;
                }
            } else
                return true;

        } catch (Exception e) {
            logger.warn(message);
        }

        return true;
    }

    public JSONObject authenticate(String token) {
        try {
            URL url = new URL("https://apitestwebphone.kavkom.com/api/v1/valid-token");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Authorization","Bearer " + token);
            conn.setRequestProperty("Content-Type","application/json");
            conn.setRequestMethod("GET");
            if(conn.getResponseCode() != 200 )
                return null;

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output;

            StringBuffer response = new StringBuffer();
            while ((output = in.readLine()) != null) {
                response.append(output);
            }
            in.close();

            return new JSONObject(response.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getKeysByValue(ConcurrentHashMap<String, WebSocket> map, WebSocket value) {
        for (ConcurrentHashMap.Entry<String, WebSocket> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
}
