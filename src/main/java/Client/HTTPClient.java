package Client;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HTTPClient {

    public JSONObject authenticate(String token) {
        try {
            URL url = new URL("https://apitestwebphone.kavkom.com/api/v1/valid-token");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Authorization","Bearer " + token);
            conn.setRequestProperty("Content-Type","application/json");
            conn.setRequestMethod("GET");
            if(conn.getResponseCode() != 200 )
                return null;

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output;

            StringBuffer response = new StringBuffer();
            while ((output = in.readLine()) != null) {
                response.append(output);
            }
            in.close();

            return new JSONObject(response.toString());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        HTTPClient client = new HTTPClient();
        JSONObject result = client.authenticate("K451i3YTTWXU06Lr2bYNzgeGEtBkiewb5wlPodxw9yvhEEFiyAy4IdY2p8Ob8PbSH30PNV");
        System.out.println(result);
    }
}
